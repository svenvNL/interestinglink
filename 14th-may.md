# Link of 14th May

## Carsten

- https://github.com/facebook/facebook-ios-sdk/issues/1374
- https://www.theverge.com/2020/5/7/21250300/eu-cookie-consent-policy-updated-guidelines-cookie-wall
- https://spectrum.ieee.org/cars-that-think/energy/batteries-storage/will-electric-cars-on-the-highway-emulate-airtoair-refueling
- https://github.com/vuejs/vite
- https://github.com/xxh/xxh
- https://engineering.fb.com/web/facebook-redesign/
- https://matrix.org/blog/2020/05/06/cross-signing-and-end-to-end-encryption-by-default-is-here
- [Async-GraphQL: A GraphQL server framework](https://github.com/async-graphql/async-graphql)
- https://github.com/carbon-app/carbon
- http://h313.info/blog/aerospace/2020/05/09/an-analysis-of-the-lego-city-deep-space-rocket.html
- https://www.faqforge.com/windows/windows-10/how-to-disable-all-of-the-built-in-windows-10-ads/
- https://github.com/GoogleChrome/web-vitals-extension
- https://changelog.com/posts/how-i-reduced-changelogs-compilation-dependencies-by-98
- https://iss-sim.spacex.com
- https://github.com/askmeegs/learn-istio
- https://github.com/hediet/vscode-drawio
- https://saltpack.org/

